GOOSE
=====

This project contains the GitLab Open Operating System Errata library, tooling and data.

Purpose & Goals
===============

Goose as a library is designed to bring together and make accessible disparate sources of information published by Operating System distributions and vendors for easier consumption by security tools.
Goose as a tool is designed to use Goose as a library to build artefacts in different formats describing the errata published by Operating System distributions and vendors for usage with other tools such as container scanners.
This Goose artefacts published by the tools are designed or for inclusion or linking in [SBOMs](https://en.wikipedia.org/wiki/Software_supply_chain) and generally for adding context around vulnerability findings present in software artifacts such as container images.

Ultimately, this data and the produced artefacts aim to reduce manual and repeat work around researching, understanding and incorporating information about errata in Operating System components which represent a dependency or layer of another artifact, such as a container or other Operating System image. This data is useful in prioritizing and planning remediation of vulnerabilities, as well as understanding and contextualizing software attack surface and dependencies.

What is an Operating System?
============================

Wikipedia defines an Operating System as:

> "system software that manages computer hardware and software resources, and provides common services for computer programs."

However for the purposes of this tooling, we are focused more on the [SDLC](https://csrc.nist.gov/glossary/term/software_development_life_cycle) and [Software Distribution](https://en.wikipedia.org/wiki/Software_distribution) aspects of an Operating System, which can also be referred to as an Operating System Distribution.

Under this definition, we also include [JeOS / Just Enough Operating System](https://en.wikipedia.org/wiki/Just_enough_operating_system) distributions such as [distroless](https://github.com/distroless) and [Wolfi](http://wolfi.dev/) which describe themselves as distroless or (un)distributions. Despite having a smaller software attack surface, projects such as these still issue errata which can be useful for contextualizing and planning remediation of security findings in these software collections.

Features
========

 - Consolidates and normalise vulnerability information and fix availability for:
   - [ ] [Red Hat Enterprise Linux (RHEL)](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux) packages
   - [ ] [Debian](https://www.debian.org/) packages
   - [ ] [Ubuntu](https://ubuntu.com/) packages
   - [ ] [Alpine](https://alpinelinux.org/) packages
   - [ ] [Wolfi](https://wolfi.dev/) packages
   - [ ] [Amazon Linux](https://aws.amazon.com/amazon-linux-2/) packages
   - [ ] [distroless](https://github.com/distroless) packages
 - [ ] Provides consistent library interface for consuming and normalising errata
 - [ ] Provides consistent library interface for consuming and normalising fix availability and remediation workflow progress
 - [ ] Allows export as [OpenVEX](https://github.com/openvex/spec/) documents for inclusion or linking in SBOMs

Usage
=====
To be written

Author
======
The GitLab Vulnerability Management team 🧡
Original GitLab author, [James Hebden](https://gitlab.com/jhebden).
