package db

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
)

//go:embed migrations/*.sql
var migrations embed.FS

type DBConnection struct {
	DB     *sql.DB
	Path    string
}

func (d *DBConnection) Migrate() error {
	if err := d.DB.Ping(); err != nil {
		return fmt.Errorf("status check on database failed: %w", err)
	}

	embed_migrations, err := iofs.New(migrations, "migrations")
	if err != nil {
		return fmt.Errorf("failed to to access migrations: %w", err)
	}

	m, err := migrate.NewWithSourceInstance("iofs", embed_migrations, fmt.Sprintf("sqlite3://%s", d.Path))
	if err != nil {
		log.Fatal(err)
	}

	return m.Up()
}

func NewDBConnection(db_path string) (*DBConnection, error) {
	db_conn := DBConnection{}
	db_conn.Path = db_path
	db, err := sql.Open("sqlite3", db_path)
	if err != nil {
		return &db_conn, err
	}

	db_conn.DB = db

	err = db_conn.Migrate()
	if err != nil && err != migrate.ErrNoChange {
		return &db_conn, err
	}

	return &db_conn, nil
}
