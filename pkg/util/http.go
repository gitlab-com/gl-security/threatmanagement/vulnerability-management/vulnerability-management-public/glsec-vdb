package util

import (
	"fmt"
	"io"
	"log/slog"
	"net/http"
)

func Fetch(url string) ([]byte, error) {
	slog.Debug("running fetch for URL", "url", url)
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	slog.Debug("HTTP response received", "url", url, "status_code", response.StatusCode, "content_length", response.ContentLength)
	bytes, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	if response.StatusCode != 200 {
		return nil, fmt.Errorf("unexpected HTTP return code %d while fetching data: %s", response.StatusCode, string(bytes))
	}
	slog.Debug("buffered ReadAll for HTTP response complete", "url", url, "size", len(bytes))
	return bytes, nil
}
