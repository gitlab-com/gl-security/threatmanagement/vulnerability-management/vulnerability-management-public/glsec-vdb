package sources

import "gitlab.com/gitlab-com/gl-security/threatmanagement/vulnerability-management/vulnerability-management-public/goose/pkg/db"

type DataSourceNVD struct {
	SourceBase
}

// create a new NVD data source
func NewSourceNVD(db *db.DBConnection) (*DataSourceNVD, error) {
	t := DataSourceNVD{
		SourceBase{
			db:   db,
			name: "NVD",
		},
	}
	return &t, nil
}

// refresh for NVD doesn't import and OS, package or fix information, just CVE baselines
