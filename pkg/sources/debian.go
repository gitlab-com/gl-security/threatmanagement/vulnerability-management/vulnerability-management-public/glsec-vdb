package sources

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"strings"
	"time"

	"gitlab.com/gitlab-com/gl-security/threatmanagement/vulnerability-management/vulnerability-management-public/goose/pkg/db"
	"gitlab.com/gitlab-com/gl-security/threatmanagement/vulnerability-management/vulnerability-management-public/goose/pkg/util"
)

type DataSourceDebian struct {
	SourceBase
}

// create a new Debian Security tracker datasource
func NewSourceDebian(db *db.DBConnection) (*DataSourceDebian, error) {
	t := DataSourceDebian{
		SourceBase{
			db:   db,
			name: "Debian GNU/Linux",
		},
	}
	return &t, nil
}

func (t *DataSourceDebian) Refresh() error {
	url := "https://security-tracker.debian.org/tracker/data/json"
	slog.Debug("downloading Debian tracker data")
	byte_data, err := util.Fetch(url)
	if err != nil {
		slog.Error("failed to fetch Debian tracker JSON data file", "url", url, "error", err)
		return err
	}
	var feed DebianTrackerData
	slog.Debug("downloaded compressed Debian, deserialising")
	err = json.Unmarshal(byte_data, &feed)
	if err != nil {
		slog.Error("failed to deserialise Debian JSON data file", "url", url, "error", err)
		return err
	}
	slog.Debug("deserialised Debian data, loading into database")
	for package_name, package_data := range feed {
		for tracker_identifier, tracker_data := range package_data {
			description := tracker_data.Description
			cve := tracker_identifier
			if !strings.HasPrefix(cve, "CVE-") {
				slog.Debug("ignoring non-CVE record in Debian feed")
				continue
			}
			// Debian tracker does not have scoring, impact or vector information
			// so, we need to lean on NVD for this information. Check if this vulnerability has been ingested via NVD.
			// If not, do a refresh for just this vuln.cur
			nvd_data, err := nvd_provider.GetOrUpdateNVDCVE(cve)
			if err != nil {
				return fmt.Errorf("failed to backfill Debian CVE %s via NVD provider: %v", err)
			}
			for release_name, release_data := range tracker_data.Releases {
				// TODO: get score, backup severity and vector from NVD?
				score := nvd_data.Score
				status := release_data.Status
				severity := safe.None[string]()
				vector := safe.None[string]()
				if nvd_data.Impact.Valid {
					severity = severity.Some(nvd_data.Impact.String)
				}
				if nvd_data.Vector.Valid {
					vector = vector.Some(nvd_data.Vector.String)
				}
				nodsa := release_data.NoDSAReason
				fixedversion := release_data.FixedVersion
				fix_state := "unknown"
				url := fmt.Sprintf("https://security-tracker.debian.org/tracker/%s", cve)
				t.Logger.Sugar().Infof("inserting Debian CVE %s (%s, %d, %v, %v) into the database", cve, description, score, severity, vector)
				db_vuln_data, err := t.DBConn.GetOrCreateVulnerabilityData(cve, description, url, score, common.VULN_SOURCE_DEBIAN, severity, vector)
				if err != nil {
					return err
				}
				t.Logger.Sugar().Debugf("loaded Debian CVE %s into database, result: %v", cve, db_vuln_data)
				// create package fix status and status is resolved
				if len(fixedversion) > 0 && status == "resolved" {
					// fixed version is set -> fixed
					fix_state = "available"
				} else if nodsa == "ignored" && status == "open" {
					// fixed version not set, NoDSA reason set to ignored, advisory is open -> wontfix
					fix_state = "wontfix"
				} else if status == "undetermined" {
					// status is undetermined, we're pending analysis -> pending
					fix_state = "pending"
				}
				// normalise fix state
				fix_state, err = common.NormaliseVulnFixStatus(fix_state)
				if err != nil {
					t.Logger.Sugar().Warnf("could not normalise Debian advisory fix state %s: %v", fix_state, err)
					continue
				}
				// normalise OS
				os, err := common.NormaliseOS("debian", "debian gnu/linux", release_name)
				if err != nil {
					t.Logger.Sugar().Warnf("failed to normalise OS during Debian tracker ingest for %s: %v", release_name, err)
					continue
				}
				t.Logger.Sugar().Debugf("normalised Debian tracker OS as %s %s", os.OS, os.Version)
				// add os to db slice
				db_os, err := t.DBConn.GetOrCreateOSFromContainerOS(os)
				if err != nil || db_os.UUID == "" {
					t.Logger.Sugar().Warnf("failed to insert DB OS during Debian tracker ingest for %s %s/%s: %v", os.OS, os.Version, release_name, err)
					continue
				}
				t.Logger.Sugar().Debugf("inserted OS for Trivy ingest into DB: %v", db_os)
				// add packages to packages db slice
				db_package, err := t.DBConn.GetOrCreatePackage(package_name, db_os.UUID)
				if err != nil {
					t.Logger.Sugar().Warnf("failed to insert DB Package during Debian ingest %s/%s: %v", package_name, os.OS, os.Version, err)
					continue
				}
				t.Logger.Sugar().Debugf("inserted package state for Trivy ingest into DB: %v", db_package)
				// add package fixes to package fixes db slice
				db_package_fix, err := t.DBConn.GetOrCreatePackageFixStatus(db_package.UUID, db_vuln_data.UUID, db_os.UUID, fix_state)
				if err != nil {
					t.Logger.Sugar().Warnf("failed to insert DB Package fix during Trivy ingest %s/%s %s - %s: %v", package_name, os.OS, os.Version, fix_state, err)
					continue
				}
				t.Logger.Sugar().Debugf("inserted package fix state for Debian ingest into DB: %v", db_package_fix)
				// ding vuln data processed metric
				metrics.IncVulnDataProcessed()
			}
		}
	}
	return nil
}

func (t *VulnerabilityDataProviderDebian) LoadData() error {
}

func (t *VulnerabilityDataProviderDebian) UpdateData() error {
	if t.DBConn != nil {

		// my favourite golang quirk
		refresh_time := time.Now().Add(-time.Hour)
		// declare as a pointer so we can tell if there was a NULL value from postgres (the pointer will be nil)
		provider_data, err := t.DBConn.GetProvider(common.VULN_SOURCE_DEBIAN)
		if err != nil {
			t.Logger.Sugar().Errorf("failed to query DB row when checking provider metadata for %s: %w", ProviderNameDebian, err)
			return err
		}
		t.Logger.Sugar().Debugf("fetching Debian security tracker data")
		if provider_data.LastUpdated.Time.Before(refresh_time) {
			t.Logger.Sugar().Infof("updating all Debian vulnerability data from Debian security tracker")
			err = t.LoadData()
			if err != nil {
				t.Logger.Sugar().Errorf("failed to load Debian data: %v", err)
				return err
			}
		} else {
			t.Logger.Sugar().Infof("skipping Debian vulnerability data refresh, DB data is new enough")
		}
		_, err = t.DBConn.UpdateProviderLastUpdated(common.VULN_SOURCE_DEBIAN)
		if err != nil {
			t.Logger.Sugar().Errorf("failed to set last run timestamp for Debian provider: %v", err)
			return err
		}
		// ding refresh metric
		metrics.IncProviderRefresh()
		return nil
	}
	return fmt.Errorf("DB is not connected during refresh of Debian data")
}
