package sources

type DebianTrackerData map[string]DebianTrackerPackage
type DebianTrackerPackage map[string]DebianTrackerEntry

type DebianTrackerEntry struct {
	Description string                              `json:"description"`
	Releases    map[string]DebianTrackerReleaseData `json:"releases"`
}

type DebianTrackerReleaseData struct {
	Status       string            `json:"status"` // open, resolved or undetermined
	FixedVersion string            `json:"fixed_version"`
	Urgency      string            `json:"urgency"`      // unimportant, low, medium, high, end-of-life, not yet assigned
	NoDSA        string            `json:"nodsa"`        // if no DSA was issued, some explanation
	NoDSAReason  string            `json:"nodsa_reason"` // why was no dsa issued (WONTFIX)
	Repositories map[string]string `json:"repositories"`
}
