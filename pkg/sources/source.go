package sources

import (
	"time"

	"gitlab.com/gitlab-com/gl-security/threatmanagement/vulnerability-management/vulnerability-management-public/goose/pkg/db"
)

// source interface
type Source interface {
	// get source name
	Name() string
	// get source id
	ID() string
	// get last refresh
	LastRefresh() time.Time
	// perform refresh
	Refresh() error
}

// base attributes used by all providers
type SourceBase struct {
	// db handle
	db *db.DBConnection
	// name
	name string
	// UUID
	uuid string
	// last refresh
	lastRefresh time.Time
}

// get source name
func (s *SourceBase) Name() string {
	return s.name
}

// get source id
func (s *SourceBase) UUID() string {
	return s.uuid
}

// get last refresh
func (s *SourceBase) LastRefresh() time.Time {
	return s.lastRefresh
}
