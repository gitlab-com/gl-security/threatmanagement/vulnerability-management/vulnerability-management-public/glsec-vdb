package types

import "database/sql"

/* example VEX To help understand these concepts
 {
"@context": "https://openvex.dev/ns/v0.2.0",
"@id": "https://openvex.dev/docs/example/vex-9fb3463de1b57",
"author": "Wolfi J Inkinson",
"role": "Document Creator",
"timestamp": "2023-01-08T18:02:03.647787998-06:00",
"version": 1,
"statements": [
  {
    "vulnerability": {
      "name": "CVE-2023-12345"
    },
    "products": [
      {"@id": "pkg:apk/wolfi/git@2.39.0-r1?arch=armv7"},
      {"@id": "pkg:apk/wolfi/git@2.39.0-r1?arch=x86_64"}
    ],
    "status": "fixed"
  }
]
}

  {
    "vulnerability": {
      "name": "CVE-2023-12345",
    }
    "products": [
      {"@id": "pkg:apk/wolfi/product@1.23.0-r1?arch=armv7"}
    ],
    "status": "not_affected",
    "justification": "component_not_present",
    "impact_statement": "The vulnerable code was removed with a custom patch"
  }
*/

// source
type Source struct {
	// last update
	LastUpdated sql.NullTime
	// source UUID
	UUID string
}

// severity
type Severity string

const (
	Critical Severity = "critical"
	High     Severity = "high"
	Moderate Severity = "moderate"
	Low      Severity = "low"
)

// vulnerability (CVE or other identifier)
type Vulnerability struct {
	// identifier
	Identifier string
	// severity
	Severity Severity
	// cvss
	CVSS float32
	// cvss version
	CVSSVersion int
	// vector
	Vector string
	// url
	URL string
	// source UUID
	Source string
}

// OS
type OS struct {
	// name
	Name string
	// release/version
	Release string
	// codename
	CodeName string
	// UUID (hashed from lowercase name+release+codename to generate stable UUIDs)
	UUID string
	// source UUID
	Source string
}

// advisory-linked package
type OSPackage struct {
	// UUID
	UUID string
	// name (package name as installed)
	Name string
	// OS UUID
	OS string
	// source UUID
	Source string
}

// fix status
type FixState string

const (
	Investigating FixState = "under_investigation"
	Affected      FixState = "affected"
	NotAffected   FixState = "not_affected"
	Fixed         FixState = "fixed"
)

// status justifications
type FixStatusJustification string

const (
	ComponentNotPresent            FixStatusJustification = "component_not_present"
	VulnerableCodeNotPresent       FixStatusJustification = "vulnerable_code_not_present"
	VulnerableCodeNotInExecutePath FixStatusJustification = "vulnerable_code_not_in_execute_path"
	VulnerableCodeNotControllable  FixStatusJustification = "vulnerable_code_cannot_be_controlled_by_adversary"
	InlineMitigationsExist         FixStatusJustification = "inline_mitigations_already_exist"
)

// per-OS package fix status
type OSPackageFixStatus struct {
	// vulnerability uuid
	// package uuid
	// fixed in version
	FixedIn string
	// state
	State FixState
	// justification
	Justification FixStatusJustification
}
