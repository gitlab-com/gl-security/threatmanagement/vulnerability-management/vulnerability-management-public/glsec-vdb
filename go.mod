module gitlab.com/gitlab-com/gl-security/threatmanagement/vulnerability-management/vulnerability-management-public/goose

go 1.21.1

require (
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
)
